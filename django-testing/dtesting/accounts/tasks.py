# create a dependency with the main project
from dtesting.celery import app
# does not create a dependency with the main project
from celery import shared_task


@app.task
def success_register_email_task():
    pass


@shared_task
def test_task_account():
    return 1 + 2


@shared_task
def add(x, y):
    return x + y


@shared_task
def mul(x, y):
    return x * y


@shared_task
def xsum(numbers):
    return sum(numbers)
